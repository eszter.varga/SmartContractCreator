"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _web = require("./web3");

var _web2 = _interopRequireDefault(_web);

var _TemplateFactory = require("./build/TemplateFactory.json");

var _TemplateFactory2 = _interopRequireDefault(_TemplateFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Tell web3 that a deployed copy of TemplateFactory exist
//on address 0xbFc1EC8AE7BFDf57087c2422a4cE143dB6c47E06

var instance = new _web2.default.eth.Contract(JSON.parse(_TemplateFactory2.default.interface), "0xd3df1C0dbd0b870A2371976529CC46cd4D045efc");

exports.default = instance;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVua25vd24iXSwibmFtZXMiOlsiaW5zdGFuY2UiLCJldGgiLCJDb250cmFjdCIsIkpTT04iLCJwYXJzZSIsImludGVyZmFjZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBR0E7Ozs7QUFDQTs7Ozs7O0FBSkE7QUFDQTs7QUFLQSxJQUFJQSxXQUFXLElBQUksY0FBS0MsR0FBTCxDQUFTQyxRQUFiLENBQXNCQyxLQUFLQyxLQUFMLENBQVcsMEJBQWdCQyxTQUEzQixDQUF0QixFQUE2RCw0Q0FBN0QsQ0FBZjs7a0JBRWVMLFEiLCJmaWxlIjoidW5rbm93biIsInNvdXJjZXNDb250ZW50IjpbIi8vVGVsbCB3ZWIzIHRoYXQgYSBkZXBsb3llZCBjb3B5IG9mIFRlbXBsYXRlRmFjdG9yeSBleGlzdFxuLy9vbiBhZGRyZXNzIDB4YkZjMUVDOEFFN0JGRGY1NzA4N2MyNDIyYTRjRTE0M2RCNmM0N0UwNlxuXG5pbXBvcnQgd2ViMyBmcm9tIFwiLi93ZWIzXCI7XG5pbXBvcnQgVGVtcGxhdGVGYWN0b3J5IGZyb20gXCIuL2J1aWxkL1RlbXBsYXRlRmFjdG9yeS5qc29uXCI7XG5cbnZhciBpbnN0YW5jZSA9IG5ldyB3ZWIzLmV0aC5Db250cmFjdChKU09OLnBhcnNlKFRlbXBsYXRlRmFjdG9yeS5pbnRlcmZhY2UpLCBcIjB4ZDNkZjFDMGRiZDBiODcwQTIzNzE5NzY1MjlDQzQ2Y2Q0RDA0NWVmY1wiKTtcblxuZXhwb3J0IGRlZmF1bHQgaW5zdGFuY2U7Il19
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVua25vd24iXSwibmFtZXMiOlsiaW5zdGFuY2UiLCJldGgiLCJDb250cmFjdCIsIkpTT04iLCJwYXJzZSIsImludGVyZmFjZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBR0E7Ozs7QUFDQTs7Ozs7O0FBSkE7QUFDQTs7QUFLQSxJQUFJQSxXQUFXLElBQUksY0FBS0MsR0FBTCxDQUFTQyxRQUFiLENBQXNCQyxLQUFLQyxLQUFMLENBQVcsMEJBQWdCQyxTQUEzQixDQUF0QixFQUE2RCw0Q0FBN0QsQ0FBZjs7a0JBRWVMLFEiLCJmaWxlIjoidW5rbm93biJ9