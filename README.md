# SmartContractCreator

## Linux installation

**Open your terminal**

Install Node Package Manager:
**sudo apt-get install npm**

Install curl:
**sudo apt-get install curl**

Install Node.js:
**curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -**
**sudo apt-get install -y nodejs**

Go to the project folder:
**cd SolidityTemplateWizzard**

Install Next:
**npm install --save next-routes**

Start the project:
**npm rum dev**

If you get **„No "from" address specified in neither the given options, nor the default options.”** when you try to create a new contract, you need to install metamask:
**https://medium.com/@followcoin/how-to-install-metamask-88cbdabc1d28**

Get free test ether to your rinkeby account:
**https://faucet.rinkeby.io/**

## How to restart the project after Solidity code modifications

#### If you make modifications in the solidity code the contarcts have to be recompiled, before you restart the project. This section presents how you can do it.

Make modifications in solidity code

Save the changed solidity file

Stop the running server:
In terminal where the server runs **ctrl + c** command

This question appears in terminal: **Terminate batch job (Y/N)?**
Type **Y** and hit enter

Go to ethereum folder in the project:
**cd ethereum**

Compile the new solidity code:
**node compile.js**

Check your ethereum/build folder, if it is empty, then you have error(s) in your solidity code.
Use **https://remix.ethereum.org** to test and debug your code.

Deploy the TemplateFactory contract:
**node deploy.js**

**Save the address where the contract is deployed to**

**Open ethereum/factory.js and replace address with the new one and save it.**

Go back to the project root folder:
**cd ..**

Run the server:
**npm run dev**
