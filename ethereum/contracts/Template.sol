pragma solidity ^0.4.17;

contract TemplateFactory {
    address[] public deployedContracts;

    function createContract(
      //---------------------------------------------------------------------------------------------
      string currentType, string currentName, string currentSymbol,
      uint currentValue, string currentExpirationDate, address currentTokenHolderWalletAddress
      //---------------------------------------------------------------------------------------------
      ) public {

        //---------------------------------------------------------------------------------------------
        address newContract = new Template(
          currentType,
          currentName,
          currentSymbol,
          currentValue,
          currentExpirationDate,
          currentTokenHolderWalletAddress
          );
          //---------------------------------------------------------------------------------------------

        deployedContracts.push(newContract);
    }

    function getDeployedContracts() public view returns (address[]) {
        return deployedContracts;
    }
}

contract Template {
    string public contractType;
    string public name;
    string public symbol;
    uint public value;
    string public expirationDate;
    address public tokenHolderWalletAddress;


    function Template(string currentType, string currentName, string currentSymbol,
      uint currentValue, string currentExpirationDate, address currentTokenHolderWalletAddress) public {
        contractType = currentType;
        name = currentName;
        symbol = currentSymbol;
        value = currentValue;
        expirationDate = currentExpirationDate;
        tokenHolderWalletAddress = currentTokenHolderWalletAddress;
    }

    function getSummary() public view returns(
      string, string, string, uint, string, address
      ) {
        return(
          contractType,
          name,
          symbol,
          value,
          expirationDate,
          tokenHolderWalletAddress
        );
    }
}


interface Provenance {
  function verify(address) public view returns (bool);
}

contract Card {
  event Transfer(address indexed Recipient, uint256 Amount);

  address public root;
  uint256 public nonce;
  address public owner;
  address public newowner;
  uint256 public balance;
  bytes32 public setup;

  function acceptsRefill() private view returns (bool) {
    return (setup & 1) != 0;
  }

  function acceptsTransfer() private view returns (bool) {
    return (setup & 2) != 0;
  }

  constructor(
    uint256 _nonce,
    address _owner,
    uint256 _balance
  ) public {
    root = msg.sender;
    nonce = _nonce;
    owner = _owner;
    balance = _balance;
  }

  function transfer(address _recipient, uint256 _debit) public {
    require(owner == msg.sender);
    require(Provenance(root).verify(_recipient));
    require(balance >= _debit);
    balance = balance - _debit;
  }

  function credit(uint256 _credit) public {
    require(balance + _credit > balance);
    balance = balance + _credit;
  }

  function handover(address _newowner) public {
    require(msg.sender == owner);
    newowner = _newowner;
  }

  function accept() public {
    require(msg.sender == newowner);
    owner = msg.sender;
    newowner = 0x0;
  }
}

contract Token is Provenance {
  event CardIssued(address indexed Sender, address Card, uint256 Nonce);
  event TokenMinted(address indexed Card, uint256 amount);

  uint256 private txcount;
  address issuer;

  constructor() public {
    issuer = msg.sender;
    txcount = 1;
  }

  function issueCard(address _owner, uint256 _balance) public {
    require(msg.sender == issuer);
    address child = new Card(txcount, _owner, _balance);
    emit CardIssued(msg.sender, child, txcount);
    if (_balance > 0)
      emit TokenMinted(child, _balance);
    txcount++;
  }

  function refillCard(address _card, uint256 _amount) public {
    require(msg.sender == issuer);
    require(_amount > 0);
    require(verify(_card));
    Card(_card).credit(_amount);
    emit TokenMinted(_card, _amount);
  }

  function childAddress(uint256 _nonce) private view returns (address) {
    if(_nonce <= 0x7f)       return address(keccak256(byte(0xd6), byte(0x94), address(this), byte(_nonce)));
    if(_nonce <= 0xff)       return address(keccak256(byte(0xd7), byte(0x94), address(this), byte(0x81), uint8(_nonce)));
    if(_nonce <= 0xffff)     return address(keccak256(byte(0xd8), byte(0x94), address(this), byte(0x82), uint16(_nonce)));
    if(_nonce <= 0xffffff)   return address(keccak256(byte(0xd9), byte(0x94), address(this), byte(0x83), uint24(_nonce)));
    if(_nonce <= 0xffffffff) return address(keccak256(byte(0xda), byte(0x94), address(this), byte(0x84), uint32(_nonce)));
    revert(); // more than 2^32 nonces not realistic
  }

  function verify(address _child) public view returns (bool) {
    require (Card(_child).root() == address(this));
    require (childAddress(Card(_child).nonce()) == _child);
    return true;
  }
}
