const HDWalletProvider = require("truffle-hdwallet-provider");
const Web3 = require("web3");

//const { interface, bytecode } = require('./compile');
const compiledFactory = require("./build/TemplateFactory.json");

//TODO update provider
const provider = new HDWalletProvider(
	"luxury black arm poet oyster outer unusual reopen diamond connect evoke item",
	"https://rinkeby.infura.io/t0f6YPjKsE32PjDdFKWk"
);

const web3 = new Web3(provider);
const deploy = async () => {
	const accounts = await web3.eth.getAccounts();

	console.log("Attempting to deploy from account", await accounts[0]);

	const result = await new web3.eth.Contract(
		JSON.parse(compiledFactory.interface)
	)
		.deploy({ data: "0x" + compiledFactory.bytecode })
		.send({ gas: "2000000", from: accounts[0] });

	console.log("Contract deployed to:", result.options.address);
};
deploy();
