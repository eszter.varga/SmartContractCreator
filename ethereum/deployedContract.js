import web3 from "./web3";
import Template from "./build/Template.json";

export default address => {
	return new web3.eth.Contract(JSON.parse(Template.interface), address);
};
