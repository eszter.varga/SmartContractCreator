//Tell web3 that a deployed copy of TemplateFactory exist
//on address 0xbFc1EC8AE7BFDf57087c2422a4cE143dB6c47E06

import web3 from "./web3";
import TemplateFactory from "./build/TemplateFactory.json";

const instance = new web3.eth.Contract(
	JSON.parse(TemplateFactory.interface),
	"0xd3df1C0dbd0b870A2371976529CC46cd4D045efc"
);

export default instance;
