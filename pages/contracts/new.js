import React, { Component } from "react";
import { Form, Button, Input, Message } from "semantic-ui-react";
import Layout from "../../components/Layout";
import factory from "../../ethereum/factory";
import web3 from "../../ethereum/web3";
import { Router } from "../../routes";

class TemplateWizzardNew extends Component {
	state = {
		//---------------------------------------------------------------------------------------------
		contractType: "",
		name: "",
		symbol: "",
		value: "",
		expirationDate: "",
		tokenHolderWalletAddress: "",
		errorMessage: "",
		loading: false
		//---------------------------------------------------------------------------------------------
	};

	onSubmit = async event => {
		event.preventDefault();

		this.setState({ loading: true, errorMessage: "" });

		try {
			const accounts = await web3.eth.getAccounts();
			await factory.methods
				.createContract(
					//---------------------------------------------------------------------------------------------
					this.state.contractType,
					this.state.name,
					this.state.symbol,
					this.state.value,
					this.state.expirationDate,
					this.state.tokenHolderWalletAddress
					//---------------------------------------------------------------------------------------------
				)
				.send({
					from: accounts[0]
				});

			Router.pushRoute("/");
		} catch (err) {
			this.setState({
				errorMessage: err.message
			});
		}

		this.setState({ loading: false });
	};

	render() {
		return (
			<Layout>
				<h3>Create a Contract</h3>

				<Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
					//---------------------------------------------------------------------------------------------
					<Form.Field>
						<label>Contract Type</label>
						<Input
							label="e.g. Gift Card"
							labelPosition="right"
							value={this.state.contractType}
							onChange={event =>
								this.setState({ contractType: event.target.value })
							}
						/>
					</Form.Field>
					<Form.Field>
						<label>Name</label>
						<Input
							label="e.g. 5000 Ft Madach Gift Card"
							labelPosition="right"
							value={this.state.name}
							onChange={event => this.setState({ name: event.target.value })}
						/>
					</Form.Field>
					<Form.Field>
						<label>Symbol</label>
						<Input
							label="e.g. MGIFTC"
							labelPosition="right"
							value={this.state.symbol}
							onChange={event => this.setState({ symbol: event.target.value })}
						/>
					</Form.Field>
					<Form.Field>
						<label>Value</label>
						<Input
							label="e.g. 5000"
							labelPosition="right"
							value={this.state.value}
							onChange={event => this.setState({ value: event.target.value })}
						/>
					</Form.Field>
					<Form.Field>
						<label>Expiration Date</label>
						<Input
							label="e.g. 2019/12/31"
							labelPosition="right"
							value={this.state.expirationDate}
							onChange={event =>
								this.setState({ expirationDate: event.target.value })
							}
						/>
					</Form.Field>
					<Form.Field>
						<label>Token Holder Wallet Address</label>
						<Input
							label="e.g. dC14D665F233c07F31158790C7BE6549BcAE2489"
							labelPosition="right"
							value={this.state.tokenHolderWalletAddress}
							onChange={event =>
								this.setState({ tokenHolderWalletAddress: event.target.value })
							}
						/>
					</Form.Field>
					//---------------------------------------------------------------------------------------------
					<Message error header="Oops!" content={this.state.errorMessage} />
					<Button loading={this.state.loading} primary>
						Create!
					</Button>
				</Form>
			</Layout>
		);
	}
}

export default TemplateWizzardNew;
