import React, { Component } from "react";
import { Card, Grid, Button } from "semantic-ui-react";
import Layout from "../../components/Layout";
import deployedContract from "../../ethereum/deployedContract";
import web3 from "../../ethereum/web3";
import { Link } from "../../routes";

class TemplateWizzardShow extends Component {
	static async getInitialProps(props) {
		const contract = deployedContract(props.query.address);

		const summary = await contract.methods.getSummary().call();

		return {
			address: props.query.address,
			//---------------------------------------------------------------------------------------------
			contractType: summary[0],
			name: summary[1],
			symbol: summary[2],
			value: summary[3],
			expirationDate: summary[4],
			tokenHolderWalletAddress: summary[5]
			//---------------------------------------------------------------------------------------------
		};
	}

	renderCards() {
		const {
			//---------------------------------------------------------------------------------------------
			contractType,
			name,
			symbol,
			value,
			expirationDate,
			tokenHolderWalletAddress
			//---------------------------------------------------------------------------------------------
		} = this.props;

		const items = [
			//---------------------------------------------------------------------------------------------
			{
				header: contractType,
				meta: "Contract type",
				description: "Template type which used to generate the contract."
			},
			{
				header: name,
				meta: "Contract name",
				description: "Long name of the contract."
			},
			{
				header: symbol,
				meta: "Symbol",
				description: "Short name of the contract."
			},
			{
				header: value,
				meta: "Money stored on the contract",
				description:
					"The balance is how much money on this contract has left to spend."
			},
			{
				header: expirationDate,
				meta: "Expiration Date",
				description:
					"The money stored in the contract can be spent until this date."
			},
			{
				header: tokenHolderWalletAddress,
				meta: "Token Holder Wallet Address",
				description: "Address of the issuing organization.",
				style: { overflowWrap: "break-word" }
			}
		];
		//---------------------------------------------------------------------------------------------
		return <Card.Group items={items} />;
	}

	render() {
		return (
			<Layout>
				<h3>Contract Details</h3>
				<Grid>
					<Grid.Row>
						<Grid.Column width={16}>{this.renderCards()}</Grid.Column>
					</Grid.Row>

					<Grid.Row>
						<Grid.Column>
							<Link route={`/`}>
								<a>
									<Button primary>Back</Button>
								</a>
							</Link>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</Layout>
		);
	}
}

export default TemplateWizzardShow;
