const routes = require("next-routes")();

routes
	.add("/contracts/new", "/contracts/new")
	.add("/contracts/:address", "/contracts/show");

module.exports = routes;
