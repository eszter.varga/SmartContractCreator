const assert = require("assert");
const ganache = require("ganache-cli");
const Web3 = require("web3");
const web3 = new Web3(ganache.provider());

const compiledFactory = require("../ethereum/build/TemplateFactory.json");
const compiledTemplate = require("../ethereum/build/Template.json");

let accounts;
let factory;
let templateAddress;
let template;

beforeEach(async () => {
	accounts = await web3.eth.getAccounts();

	factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
		.deploy({ data: compiledFactory.bytecode })
		.send({ from: accounts[0], gas: "1000000" });

	await factory.methods
		.createContract(
			"Gift Card",
			"5000 Ft Madach Gift Card",
			"MGIFTC",
			5000,
			"2019/12/31",
			accounts[0]
		)
		.send({
			from: accounts[0],
			gas: "1000000"
		});

	[templateAddress] = await factory.methods.getDeployedContracts().call();
	template = await new web3.eth.Contract(
		JSON.parse(compiledTemplate.interface),
		templateAddress
	);
});

describe("Template Wizzard", () => {
	it("deploys a factory and a template", () => {
		assert.ok(factory.options.address);
		assert.ok(template.options.address);
	});
});
